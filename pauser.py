#!/usr/bin/env python3

import os
import sys
import json
import time
import datetime
from systemd import journal

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gio, GLib

from idle_time import IdleMonitor

pause_after_minutes=20
idle_reset_minutes=1
postpone_minutes=7
needle="Starting Fingerprint Authentication Daemon"
config_file = str(os.getenv("HOME")) + "/.config/pauser.conf"
state_file="/tmp/pauser.json"
state=None
default_state={"last_pause": int(time.time()) }


def get_idle ():
    monitor = IdleMonitor.get_monitor()
    return int(monitor.get_idle_time())

def get_dnd ():
    setting = Gio.Settings.new("org.gnome.desktop.notifications")
    return not setting.get_value("show-banners").get_boolean()

def load_state(filepath):
    if os.path.isfile(filepath):
        with open (filepath, mode="r", encoding="utf-8") as f:
            return json.load(f)
    # Return default
    return default_state

def save_state(filepath, data):
    with open (filepath, mode="w", encoding="utf-8") as f:
        json.dump(data, f)

def minutes_since(timestamp):
    return int((time.time()-timestamp)/60)

def parse_args():
    global state

    interval = str(sys.argv[0]).split(".")

    if len(sys.argv) > 1:
        if str(sys.argv[1]).strip() == "--reset":
            # Reset state, as in a pause has just been had.
            save_state(state_file, default_state)
            print("Pause reset!")
            sys.exit(0)

        elif str(sys.argv[1]).strip() == "--postpone":
            # Postpone next pause to {postpone_minutes} from now. Done by using now(), subtracting pause time, adding 5 mins.
            state = load_state(state_file)
            #print(state, state['last_pause'])
            state['last_pause'] = time.time()-(pause_after_minutes*60)+(postpone_minutes*60)
            #print(state, state['last_pause'])
            save_state(state_file, state)
            print(f"Pause postponed to {postpone_minutes} minutes from now!")
            sys.exit(0)

def unlocked_minutes():
    """ Return last time session was unlocked, from journal logs.
        Similar to
        
        # /usr/bin/journalctl --since -1h |grep "Starting Fingerprint Authentication Daemon"
    """

    j = journal.Reader()
    j.seek_realtime(time.time()- pause_after_minutes*60)

    filtered_entries = []
    for entry in j:
        if needle in entry['MESSAGE']:
            filtered_entries.append(entry)

    if len(filtered_entries) == 0:
        return -1
    return int((datetime.datetime.now()-filtered_entries.pop()['__REALTIME_TIMESTAMP']).total_seconds() / 60.0)

def save_day_start(state):
    """ Start day """

    if "first_active" in state.keys():
        # Check if active _today_

        first_active = datetime.datetime.fromtimestamp(int(state["first_active"]))
        today = datetime.datetime.now()
        
        # Update timestamp
        if first_active.day != today.day:
            state["first_active"] = time.time()

    else:
        state["first_active"] = time.time()

    return state

def load_config():
    config={
        "pause_after_minutes": 20,
        "idle_reset_minutes": 1,
        "needle": "Starting Fingerprint Authentication Daemon"
    }

    if os.path.isfile(config_file):
        with open (config_file, mode="r", encoding="utf-8") as f:
            config = json.load(f)
        return config["pause_after_minutes"], config["idle_reset_minutes"], config["needle"]

    # Save default
    save_config(config_file, config["pause_after_minutes"], config["idle_reset_minutes"], config["needle"])
    return config["pause_after_minutes"], config["idle_reset_minutes"], config["needle"]

def save_config(filepath, pause_after_minutes, idle_reset_minutes, needle):
    config={
        "pause_after_minutes": pause_after_minutes,
        "idle_reset_minutes": idle_reset_minutes,
        "needle": needle
    }
    with open (filepath, mode="w+", encoding="utf-8") as f:
        json.dump(config, f)


pause_after_minutes, idle_reset_minutes, needle = load_config()

state=load_state(filepath=state_file)
state=save_day_start(state=state)

parse_args()

idle_since=get_idle()
dnd=get_dnd()
output=""

if dnd:
    output += f"🔕 DND"

else:
    # Reset if recenly unlocked
    unlocked = unlocked_minutes()
    if unlocked != -1:
        state["last_pause"] = int(time.time())-unlocked*60
        output += f"Last unlock {minutes_since(state['last_pause'])}m ago ({pause_after_minutes}m)"

    # Reset if user is idling
    elif (idle_since/60) > idle_reset_minutes:
        state["last_pause"] = int(time.time())
        output += f"🛌 Idle {idle_since}s"

    # Complain if break is due
    elif minutes_since(state['last_pause']) > pause_after_minutes:
        output += f"Time for a break ❗\n{minutes_since(state['last_pause'])}m since pause ({pause_after_minutes}m) ❗"

    # Default state. Active.
    else:
        output += f"{minutes_since(state['last_pause'])}m/{pause_after_minutes}m"

print(f"""{output}
---
🛌 Idle for {idle_since}s
⏳ Last pause {minutes_since(state['last_pause'])}m ago ({pause_after_minutes}m)
🐓 First active today {datetime.datetime.fromtimestamp(int(state["first_active"])).strftime("%H:%M")}
---
⏲  Postpone break {postpone_minutes} minutes | bash='{sys.argv[0]} --postpone' terminal=false
🛑 Reset state | bash='{sys.argv[0]} --reset' terminal=false
   Config file | iconName=gedit bash="/usr/bin/gedit {config_file}" terminal=false""")

save_state(filepath=state_file, data=state)
