# TODO

- Config file ✅
- Collect stats
  - start of day ✅
  - time spent idle: impossible
  - time spent active: impossible
- Configurable output
- "Postpone break to X minutes from now" ✅
